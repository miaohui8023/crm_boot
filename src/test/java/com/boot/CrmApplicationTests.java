package com.boot;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrmApplicationTests {

	@Test
	public void contextLoads() {
	}

	protected MockMvc mockMvc;
	@Autowired
	private WebApplicationContext wac;

	//这个方法在每个方法执行前都会执行一遍
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

}
