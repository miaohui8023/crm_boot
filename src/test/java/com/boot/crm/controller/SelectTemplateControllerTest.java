package com.boot.crm.controller;

import com.boot.CrmApplicationTests;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: ys
 * @Date:Create in 2018/6/21 15:11
 * @Description:
 */
public class SelectTemplateControllerTest extends CrmApplicationTests {
    @Test
    public void getAll() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/template/getAllByPage/0/10")     //请求的url，，请求的方法是get请求
                .param("test","test的值"))           //添加参数
                .andExpect(MockMvcResultMatchers.status().isOk())   //返回的状态是20
                .andDo(MockMvcResultHandlers.print())               //打印出请求和相应的内容
                .andReturn();
        System.out.println("结果:"+result.getResponse().getContentAsString());    //将相应的格式转化成字符串
    }



}
