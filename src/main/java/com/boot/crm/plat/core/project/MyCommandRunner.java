package com.boot.crm.plat.core.project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by ys on 2018/8/11.
 * https://blog.csdn.net/wohaqiyi/article/details/80580165
 * springBoot设置项目打开后自动打开浏览器运行项目
 */
//@Component
public class MyCommandRunner implements CommandLineRunner {
        //https://blog.csdn.net/wohaqiyi/article/details/80580165
    @Value("${spring.auto.openUrl}")
    private boolean openUrl;
    @Value("${spring.web.googleExecute}")
    private String googleExecute;
    @Value("${spring.web.loginUrl}")
    private String loginUrl;
    private static final Logger logger = LoggerFactory.getLogger(MyCommandRunner.class);
    @Override
    public void run(String... strings) throws Exception {
        if (openUrl){
            String targetPath = googleExecute +" "+ loginUrl;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(targetPath);
                logger.debug("启动浏览器项目成功");//如果日志级别设置为info此句就不会被打印
            }catch (Exception e){
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
    }
}
