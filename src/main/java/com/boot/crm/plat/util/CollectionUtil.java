package com.boot.crm.plat.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Author: ys
 * @Date:Create in 2018/7/30 15:02
 * @Description:
 */
public class CollectionUtil {
    /**
     * 方式1：数组 转化成 list
     * @return list集合
     */
    public static List<String> arrayConvertToList(String[] strings1){
        String [] strings = new String[]{"1","2","3","4"};
        List<String> list = Arrays.asList(strings); //此处的list不能进行删除和添加操作，否则报错
        List<String> list1 = new ArrayList<>(list);
        return list1;
    }
    /**
     * 方式2：数组 转化成 list
     * @return list集合
     */
    public static List<String> arrayConvertToListType2(String[] strings1){
        String [] strings = new String[]{"1","2","3","4"};
        List<String> list = new ArrayList<>(strings.length);
        Collections.addAll(list,strings);
        return list;
    }
    public static String[] listConvertToArray(){
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        String [] strings = new String[list.size()];
        strings = list.toArray(strings);
        //数组转化成字符串：[1, 2, 3]
        String s = Arrays.toString(strings);
        return strings;
    }

    public static void main(String[] args) {
//        List<String> list = arrayConvertToListType2(new String[]{});
        String[] strings = listConvertToArray();
        System.out.println(strings);
    }
}
