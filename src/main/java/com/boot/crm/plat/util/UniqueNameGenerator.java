package com.boot.crm.plat.util;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;

/**
 * spring 提供两种生成beanName生成策略
 * 基于注解的spring-boot默认使用的是AnnotationBeanNameGenerator，它生成beanName的策略就是取当前类名(不是全限定类名)作为beanName
 * 如果出现不同包结构下同样的类名，肯定会出现冲突
 * 此类的作用是
 * Created by ys on 2018/8/3 10:39
 *
 */
public class UniqueNameGenerator extends AnnotationBeanNameGenerator{
    @Override
    public String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry) {
        //全限定类名
        String beanName = definition.getBeanClassName();
        return beanName;
    }
}
