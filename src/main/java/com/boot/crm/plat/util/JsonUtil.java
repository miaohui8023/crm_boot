package com.boot.crm.plat.util;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * Created by ys on 2018/8/18.
 * JSON类型的转换
 */
public class JsonUtil {

    private static JacksonAdapter objectMapper;

    public static JsonNode jsonToObj(String json){
        try {
            return initMapper().readTree(json);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static <T> T jsonToObj(String json, Class<T> clazz){
        if (!UtilTool.isValidString(json)){
            return null;
        }else {
            try {
                return initMapper().readValue(json,clazz);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    public static <T>T jsonToObj(String json, Class<T> clazz, Class... element){
        if (!UtilTool.isValidString(json)){
            return null;
        }else {
            try {
                return initMapper().readValue(json, initMapper().createType(clazz,element));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private static JacksonAdapter initMapper(){
        if (null == objectMapper){
            objectMapper = new JacksonAdapter();
        }
        return objectMapper;
    }
}
