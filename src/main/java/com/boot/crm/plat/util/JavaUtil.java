package com.boot.crm.plat.util;

/**
 * @Author: ys
 * @Date:Create in 2018/7/30 17:27
 * @Description:
 */
public class JavaUtil {
    public static void getForeach1(){
        for (int i=0;i<5;i++){
            System.out.print((i+1)+" , ");//1,2,3,4,5
        }
    }
    public static void getForeach2(){
        System.out.println();
        for (int i=0;i<5;i++){
            System.out.print((i++)+" , ");//0,2,4
        }
    }
    public static void getForeach3(){
        System.out.println();
        for (int i=0;i<5;i++){
            System.out.print((++i)+" , ");//1,3,5
        }
    }

    public static void main(String[] args) {
        getForeach1();
        getForeach2();
        getForeach3();
    }
}
