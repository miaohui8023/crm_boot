package com.boot.crm.plat.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: ys
 * @Date:Create in 2018/6/27 16:22
 * @Description:
 */
public class TimeUtil {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 针对数据库时间类型为：timeStamp的(尚未验证)
     * 有时给时间类型字段(Date,Timestamp)用set赋值的时候，
     * 获取当前时间，类型是TimeStamp
     * @return
     * @throws ParseException
     */
    public static Timestamp getNowTime() throws ParseException {
        String format = sdf.format(new Date());
        Date parse = sdf.parse(format);
        return new Timestamp(parse.getTime());
    }

    /**
     * 针对数据库时间类型为：date/datetime的
     * 获取当前时分秒 :
     * @return 当前时分秒
     */
    public static Date getCurrentTime(){
        Date date = null;
        try {
            date = sdf.parse(sdf.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;

    }
}
