package com.boot.crm.annotation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @program: crm_boot
 * @author: yangshen
 * @description: 记录实体类中会用到的注解
 * @create: 2020-05-27 14:22
 **/
@Data
public class Entity {

    /**
     * 主键
     */
    @Id
    private String id;

    /**
     * 返回给调用者的字段名
     */
    @JsonProperty("ckf002")
    private String userName;

    /**
     * mongodb 保存到数据库时 保存的字段名
     */
    @Field("user_age")
    private String userAge;

}
