package com.boot.crm.controller.bootstrapmaster;

import com.boot.crm.doMain.Template;
import com.boot.crm.plat.util.JsonUtil;
import com.boot.crm.service.template.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by ys on 2018/8/8.
 */
@Controller
@RequestMapping("/master")
public class MasterController {
    @Resource(name = "templateServiceImpl")
    private TemplateService templateService;


    @RequestMapping("/showMasterMainPage")
    public ModelAndView showMasterMainPage2(){
        return new ModelAndView("practice/bootstrapTable-master/page/userManagement");
    }

    //首页的查询 和 查询按钮的条件查询
    @RequestMapping("/getAllTemplate")
    @ResponseBody
    public List<Template> getAllTemplate(@RequestParam Map<String,Object> map,Template template) {
        return templateService.getAll(map);
    }
    @RequestMapping("/updateTemplate")//
    @ResponseBody
    public boolean updateTemplate(Template template){
        return templateService.updateTemplate(template);
    }
    @RequestMapping("/insertTemplate")
    @ResponseBody
    public boolean insertTemplate(Template template){
        return templateService.insertTemplate(template);
    }
    @RequestMapping("/deleteTemplate")
    @ResponseBody
    public int deleteTemplate(String rowIds){
        List list = JsonUtil.jsonToObj(rowIds, List.class, String.class);
        return templateService.delete(list);
    }
}
