package com.boot.crm.controller.template;

import com.boot.crm.plat.util.JsonUtil;
import com.boot.crm.plat.util.UtilTool;
import com.boot.crm.service.template.TemplateService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Author: ys
 * @Date:Create in 2018/7/13 16:36
 * @Description:
 */
@Controller
@RequestMapping("/insetTemplate")
public class InsertTemplateController {
    @Resource(name = "templateServiceImpl")
    private TemplateService templateService;

    @RequestMapping("/insert")
    @ResponseBody
    public void insertTemplate(@RequestParam Map<String,Object> map){
        String data =(String) map.get("data");
        List list = JsonUtil.jsonToObj(data, List.class);
        Map<String,Object> map1 =(Map<String,Object>) list.get(0);
//        int i = templateService.insertTemplate(map1);
    }
    @RequestMapping("/deleteTemplate")
    @ResponseBody
    public void deleteTemplate(String id){
        String[] split = id.split(",");
        int delete = templateService.delete(Arrays.asList(split));
    }
}
