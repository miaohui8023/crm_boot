package com.boot.crm.dao;

import com.boot.crm.doMain.Template;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TemplateMapper {
    /**
     * 注意：@param("值"):此注解适应于xml中直接使用foreach循环
     * 若是直接引用map的key，则不用@Param注解(若是引用@param注解则获取不到值)，如下getAll方法
     */
    List<Template> getAll(Map<String,Object> map);
    List<Template> getListMapTemplate();


    //新增
    int insertTemplate(Template template);

    //删除
    int deleteTemplate(List list);

    //修改
    //@Param用于foreach循环，若不循环需template.templateCode来取值
    int updateTemplate(@Param("template") Template template);

}