package com.boot.crm.java.interfanceTest;

/**
 * Created by ys on 2018/8/6 14:23
 */
public class MyClass2 implements MyInterface {
    @Override
    public String getName(String name) {
        return "class2:"+name;
    }

    @Override
    public String getAddress(String address) {
        return "class2:"+address;
    }
}
