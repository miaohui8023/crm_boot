package com.boot.crm.java.interfanceTest;

/**
 * Created by ys on 2018/8/6 14:07
 */
public class Main {
    public static void main(String[] args) {
        MyInterface myInterfance = new MyClass();
        String shangHai = myInterfance.getAddress("上海");
        System.out.println(shangHai);
        MyInterface myInterface = getyInterface();
        //接口可以引用实现类，实现类不能引用接口
        // (父类更抽象，子类更具体，子类包裹在父类外面，子类比父类更强大)
        MyInterface myClass = getyInterface1();
        MyInterface myInterface1 = getyInterface2();
        System.out.println();
    }
    private static MyInterface getyInterface(){
        return new MyClass();
    }
    private static MyClass getyInterface1(){
        return new MyClass();
    }
    private static MyInterface getyInterface2(){
        return new MyClass2();
    }
}
