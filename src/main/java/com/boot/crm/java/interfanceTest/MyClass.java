package com.boot.crm.java.interfanceTest;

/**
 * Created by ys on 2018/8/6 14:05
 */
public class MyClass implements MyInterface {
    @Override
    public String getName(String name) {
        return "class:"+name;
    }

    @Override
    public String getAddress(String address) {
        return "class:"+address;
    }
    public int getAge(int age){
        return age;
    }
}
