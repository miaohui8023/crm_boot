package com.boot.crm.java.interfanceTest;

/**
 * Created by ys on 2018/8/6 14:05
 */
public interface MyInterface {
    public String getName(String name);
    public String getAddress(String address);
}
