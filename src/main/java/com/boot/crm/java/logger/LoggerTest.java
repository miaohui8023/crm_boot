package com.boot.crm.java.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ys on 2018/8/3 9:55
 */
public class LoggerTest {
    public static final Logger logger = LoggerFactory.getLogger(LoggerTest.class);
    public static void main(String[] args) {
        logger.debug("大幅度发");
        System.out.println(logger.getName());
        inputLoggerMsg("测试................");
    }
    //在创建日志消息前，先检查当前日志级别
    public static void inputLoggerMsg(String message){
        if (logger.isDebugEnabled()){
            logger.debug(message);
        }
    }
}
