package com.boot.crm.java.collection.collection;

import com.boot.crm.doMain.Template;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * create by yangShen
 * 2019/9/11 18:43
 * 集合list的循环操作
 */
public class ListToLoop {
    public static void main(String[] args) {
        listToRemoveOrAdd();
        removeObjToException();
    }

    /**
    * 不要在 foreach 循环里进行元素的 remove/add 操作。remove 元素请使用
     * Iterator 方式，如果并发操作，需要对 Iterator 对象加锁。
    */
    public static void listToRemoveOrAdd(){
        // 正例：
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("1");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String item = iterator.next();
            if (! StringUtils.isEmpty(item)) {
//                iterator.remove();
            }
        }
        // 反例：
        for (String item : list) {
            if ("1".equals(item)) {
                list.remove(item);
            }
        }
    }

    /**
     *
     * */
    public static void removeObjToException(){
        List<Template> list = new ArrayList<>();
        Template template1 = new Template();
        template1.setCreateUser("aa");
        Template template2 = new Template();
        template2.setCreateUser("bb");
        list.add(template1);
        list.add(template2);
        for (Template template:list){
            list.remove(template);  //不能在foreach循环中删除，要用iterator
        }
    }
}
