package com.boot.crm.java.collection.collection;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ys on 2018/8/3 16:27
 *
 * 利用hashMap的key的性能，key唯一，满足了HashSet中元素不重复的特性
 * 如果添加的元素在HashSet中不存在，则返回true
 * 存在，则返回false
 *
 *
 */
public class HashSetMy {
    public static void main(String[] args) {
        Set<Object> sets = new HashSet<>();
        sets.add("set1");
    }
}
