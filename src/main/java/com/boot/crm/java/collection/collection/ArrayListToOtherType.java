package com.boot.crm.java.collection.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ys on 2018/8/3 16:27
 * 集合list转化成其他类型的操作
 */
public class ArrayListToOtherType {
    public static void main(String[] args) {

    }

    /**
     * 直接使用 toArray 无参方法存在问题，此方法返回值只能是 Object[]类，若强转其它类型数组将出
     * 现 ClassCastException 错误
     */
    public void toArray(){
        List<String> arrayList = new ArrayList<>();
        String[] objects = arrayList.toArray(new String[0]);
    }
}
