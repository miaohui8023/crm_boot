package com.boot.crm.java.collection;

import java.util.Arrays;

/**
 * Created by ys on 2018/8/6 9:48
 */
public class ArrayMy {
    public static void main(String[] args) {
    }

    /**
     * 获取数组的三种方式
     */
    private static String[] getArray(){
        String [] strings = new String[4];
        String [] strings1 = new String[]{"s1","s2","s3"};
        String [] strings2 = {"s1","s2","s3"};
        return strings;
    }
}
