package com.boot.crm.java.thread.ThreadLocal.practice1;

/**
 * Created by ys on 2019/4/20 21:25
 * 用来存放 ThreadLocal，以便存储和获取时候的 ThreadLocal一致
 */
public class ThreadLocalCache {

    public static ThreadLocal<BaseSignatureRequest> baseSignatureRequestThreadLocal = new ThreadLocal<>();

}
