package com.boot.crm.java.thread.concurrence;

public interface HelloService {
    void sayHello(long timeMillis);
}
