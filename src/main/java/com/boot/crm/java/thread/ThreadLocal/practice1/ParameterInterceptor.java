package com.boot.crm.java.thread.ThreadLocal.practice1;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ys on 2019/4/20 21:26
 * 访问场景1：GET api/users?device=android
 * 访问场景2：GET api/users?device=ios
 */
public class ParameterInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object o) throws Exception {
        String device = request.getParameter("device");
        BaseSignatureRequest baseSignatureRequest = new BaseSignatureRequest();
        baseSignatureRequest.setDevice(device);
        ThreadLocalCache.baseSignatureRequestThreadLocal.set(baseSignatureRequest);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {


    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        ThreadLocalCache.baseSignatureRequestThreadLocal.remove();
    }
}
