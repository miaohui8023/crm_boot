package com.boot.crm.java.thread.ThreadLocal.practice2;

/**
 * Created by ys on 2019/4/21 17:47
 */
public class Test {
    ThreadLocal<Long> longThreadLocal = new ThreadLocal<Long>();
    ThreadLocal<String> stringThreadLocal = new ThreadLocal<String>();

    public void set(){
        longThreadLocal.set(Thread.currentThread().getId());
        stringThreadLocal.set(Thread.currentThread().getName());
    }

    public long getLong(){
        return longThreadLocal.get();
    }

    public String getString(){
        return stringThreadLocal.get();
    }



    public static void main(String[] args) {
        Test test = new Test();
        System.out.println(test.getLong());
        System.out.println(test.getString());
    }
}
