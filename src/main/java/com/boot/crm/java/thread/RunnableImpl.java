package com.boot.crm.java.thread;

/**
 * Created by ys on 2018/3/4.
 */
public class RunnableImpl implements Runnable {

    private int ticket = 5;

    public static void main(String[] args) {
        RunnableImpl runnable = new RunnableImpl();
        Thread thread = new Thread(runnable,"窗口一");
        thread.start();
        Thread thread1 = new Thread(runnable,"窗口二");
        thread1.start();

    }

    @Override
    public void run() {
        while (true){
            System.out.println(Thread.currentThread().getName()+":"+ticket--);
            if (ticket <1){
                break;
            }
        }

    }
}
