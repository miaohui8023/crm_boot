package com.boot.crm.java.thread.ThreadLocal.practice1;

/**
 * Created by ys on 2019/4/20 21:14
 * 用来存放公共参数
 */
public class BaseSignatureRequest {

    private String device;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
