package com.boot.crm.java.thread;

/**
 * Created by ys on 2018/2/28.
 */
public class ThreadExtend extends Thread{

    private int ticket = 5;

    public ThreadExtend(String name) {
        super(name);
    }

    public static void main(String[] args) {
        Thread thread = new ThreadExtend("窗口一");
        thread.start();
        Thread thread1 = new ThreadExtend("窗口二");
        thread1.start();
    }

    @Override
    public void run() {
        while (true){
            System.out.println(this.getName()+":"+ticket--);
            if (ticket<0){
                break;
            }
        }
    }
}
