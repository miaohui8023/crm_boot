package com.boot.crm.java.thread.execute.sequence;

/**
 * create by yangShen
 * 2019/9/2 10:31
 * 通过子程序 join 使线程按顺序执行
 *
 */
public class TheadJoinDemo {
    public static void main(String[] args) {
        Thread thread11 = new Thread(new ThreadTest());


        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("产品经理规划新需求");
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    thread1.join();
                } catch (InterruptedException e) {

                }
                System.out.println("开发人员开发新需求功能");
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    thread2.join();
                } catch (InterruptedException e) {

                }
                System.out.println("开发人员开发新需求功能");
            }
        });

        System.out.println("早上：");
        System.out.println("测试人员来上班了。。。");
        thread3.start();
        System.out.println("产品经理来上班了。。。");
        thread1.start();
        System.out.println("开发人员来上班了。。。");
        thread2.start();

    }
}
class ThreadTest implements Runnable{

    @Override
    public void run() {
        System.out.println("产品经理规划新需求");
    }
}

