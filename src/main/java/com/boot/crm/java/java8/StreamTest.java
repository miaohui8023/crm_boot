package com.boot.crm.java.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author: ys
 * @Date:Create in 2018/7/26 9:40
 * @Description:
 */
public class StreamTest {
    public static void main(String[] args) {
        learnStream();
    }

    private static void learnStream(){
        //首先,创建一个1-6乱序的List
        List<Integer> lists =new ArrayList<>();
        lists.add(4);
        lists.add(3);
        lists.add(6);
        lists.add(1);
        lists.add(5);
        lists.add(2);
        Stream<Integer> stream = lists.stream();
        //最小值
        System.out.println("list中的最小值");
        Optional<Integer> min = stream.min(Integer::compareTo);
        if (min.isPresent()){
            System.out.println(min.get());
        }
        //最大值
        System.out.println("list中的最大值");
        Optional<Integer> max = lists.stream().max(Integer::compareTo);
        max.ifPresent(System.out::println);
        //排序
        System.out.println("将list进行排序");
        Stream<Integer> sorted = lists.stream().sorted();
        sorted.forEach(elem->{
            System.out.print(elem+" ");
        });
        System.out.println();
        //过滤
        System.out.println("过滤list流，只剩下那些大于3的元素");
        lists.stream()
                .filter(elem->elem>3)
                .forEach(elem-> System.out.print(elem+" "));
        System.out.println();
        //过滤
        System.out.println("过滤list流，只剩下那些大于0小于4的元素");
        lists.stream()
                .filter(elem -> elem > 0)
                .filter(elem -> elem < 4)
                .sorted(Integer :: compareTo)
                .forEach(System.out::println);

    }
}
