package com.boot.crm.java.javatest;

import java.util.Arrays;
import java.util.Collections;

/**
 * @Author: ys
 * @Date:Create in 2018/8/2 9:31
 * @Description:
 */
public class ParamMoreTest {
//    public static String getString(){
//        return "getString";
//    }
    public static String getString(String... strings){
        return "数组："+Arrays.toString(strings);
    }

    public static void main(String[] args) {
        String string = getString();
        System.out.println(string);
        String string1 = getString("arr1","arr2");
        System.out.println(string1);
    }
}
