package com.boot.crm.springBoot.webSocket.broadcast;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by ys on 2018/7/25.
 * 演示控制器
 * 1.当浏览器向服务端发送请求时,通过@MessageMapping映射/welcome这个地址,类似@RequestMapping
 * 2.当服务端有消息时,会对订阅了@SendTo中的路径的浏览器发送消息
 */
@Controller
public class WsController {
    @MessageMapping("/welcome") //1.
    @SendTo("/topic/getResponse")   //2.
    public WiselyResponse say(WiselyMessage message) throws Exception {
        Thread.sleep(300);
        return new WiselyResponse(message.getAll()+message.getName()+":"+message.getContent());
    }
}
