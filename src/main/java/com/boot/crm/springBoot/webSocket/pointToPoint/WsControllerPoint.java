package com.boot.crm.springBoot.webSocket.pointToPoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.security.Principal;

/**
 * 演示控制器
 * Created by ys on 2018/8/1.
 * 1.通过SimpMessagingTemplate 向浏览器发送消息
 * 2.在SpringMVC中，可以直接在参数中获得principal,pinciple中包含当前用户的信息
 * 3.这里是一段硬编码，如果发送人是wyf,则发送给wisely;如果发送人是wisely,则发送给wyf,
 *   读者可以根据项目实际需要改写此处代码
 * 4.通过messagingTemplate.convertAndSendToUser 向用户发送消息，
 *   第一个参数是接收消息的用户，第二个是浏览器订阅的地址，第三个是消息本身
 */
@Controller
public class WsControllerPoint {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;    //1.

    @MessageMapping("/chat")
    public void handleChat(Principal principal,String msg){ //2.
        if (principal.getName().equals("wyf")){ //3.
            messagingTemplate.convertAndSendToUser("wisely",
                    "/queue/notifications",principal.getName()+"-send  : "+msg);   //4.
        }else {
            messagingTemplate.convertAndSendToUser("wyf",
                    "/queue/notifications",principal.getName()+"-send  : "+msg);
        }
    }
}
