package com.boot.crm.springBoot.webSocket.broadcast;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

/**
 * webSocket : 广播式
 * Created by ys on 2018/7/24.
 * 1.开启webSocket支持，开启使用stomp协议来传输基于代理的消息,
 *   这时控制器支持@MessageMapping,就像使用@RequestMapping一样
 * 2.注册stomp协议的节点(endpoint),并映射指定URL
 * 3.注册一个stomp的endpoint,并指定使用SockJS协议
 * 4.配置消息代理(Message Broker)
 * 5.广播式应配置一个/topic代理
 */
@Configuration
@EnableWebSocketMessageBroker   //1.
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {    //2.
        registry.addEndpoint("/endpointWisely").withSockJS();   //3.
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {    //4.
        registry.enableSimpleBroker("/topic");  //5.
    }
}
