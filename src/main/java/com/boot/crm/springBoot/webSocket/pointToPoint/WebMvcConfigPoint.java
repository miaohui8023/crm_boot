package com.boot.crm.springBoot.webSocket.pointToPoint;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by ys on 2018/8/1.
 * WebMvcConfigurerAdapter
 *  第一个参数:指URL请求的路径
 *  第二个参数:给指定的URL的路径,指定一个html的路径(视图view)
 */
@Configuration
public class WebMvcConfigPoint extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("/springBoot/webSocket/login");
        registry.addViewController("/chat").setViewName("/springBoot/webSocket/chat");
    }
}
