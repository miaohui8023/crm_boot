package com.boot.crm.springBoot.webSocket.pointToPoint;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by ys on 2018/7/30.
 * 1.设置Spring Security 对/和/"login" 路径不拦截
 * 2.设置Spring Security的登录页面访问路径为/login
 * 3.登录成功后转向/chat路径
 * 4.在内存中分别配置两个用户wyf和wisely,密码和用户名一致，角色是USER
 * 5./resources/static 目录下的静态资源，spring security 不拦截
 * 6.参考网址：https://blog.csdn.net/shawearn1027/article/details/71119587
 *   关闭防跨域攻击，此处关闭后才可以发送post请求
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {  //放开下面的注释即可开始测试
        http.csrf().disable();  //关闭防跨域攻击 (粗暴)   //6.

//        http.authorizeRequests()
//            .antMatchers("/","/login").permitAll()  //1.
//            .anyRequest().authenticated()
//            .and()
//            .formLogin()
//            .loginPage("/login")    //2.
//            .defaultSuccessUrl("/chat") //3.
//            .permitAll()
//            .and()
//            .logout()
//            .permitAll();
    }
//    4.
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("wyf").password("wyf").roles("USER")
                .and()
                .withUser("wisely").password("wisely").roles("USER");
    }
    //5.
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/static/**");
    }
}
