package com.boot.crm.springBoot.webSocket.broadcast;

/**
 * Created by ys on 2018/7/25.
 * 浏览器向服务端发送的消息用此类接收
 */
public class WiselyMessage {
    private String name;
    private String content;
    private String all;

    public String getAll() {
        return all;
    }

    public String getContent() {
        return content;
    }

    public String getName(){
        return name;
    }
}
