package com.boot.crm.springBoot.cache;

import com.boot.crm.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by ys on 2018/9/9.
 */
@RequestMapping("/cacheController")
@RestController
public class CacheController {
    @Autowired
    @Qualifier(value = "demoServiceImpl")
    private DemoService demoService;
    @Resource
    private RedisUtil redisUtil;

    @RequestMapping("/put")
    public DemoPerson put(DemoPerson person){
        return demoService.save(person);
    }

    @RequestMapping("/able")
    public DemoPerson cacheAble(DemoPerson person){
        return demoService.findOne(person);
    }

    @RequestMapping("/evit")
    public String evit(Long id){
        demoService.remove(id);
        return "ok";
    }

    @RequestMapping(value = "/test")
    public void demoTest(){
        redisUtil.set("1","value22222");
    }

}
