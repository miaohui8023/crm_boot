package com.boot.crm.springBoot.cache;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by ys on 2018/8/26.
 * 1.@Entity注解指明这是一个和数据库映射的实体类
 * 2.@Id指明这个属性为数据库的主键
 * 3.@GeneratedValue注解默认使用 主键生成方式为自增
 */
@Entity     //1.
public class DemoPerson implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id     //2.
    @GeneratedValue     //3.
    private Long id;
    private String cacheKey;
    private String name;
    private Integer age;
    private String address;

    public DemoPerson() {
    }

    public DemoPerson(Long id, String name, Integer age, String address,String cacheKey) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.cacheKey = cacheKey;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
