package com.boot.crm.springBoot.cache;

import com.boot.crm.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by ys on 2018/9/3.
 * 3.@Cacheable:在方法执行前，spring先查看缓存中是否有数据，若有数据则直接返回缓存数据；若没有则调用方法并将方法返回值放入缓存中
 * 1.@CachePut:无论怎样，都会将方法的返回值放入缓存。@CachePut的属性与@Cacheable保持一致
 * 2.@CacheEvict:将一条或多条数据从缓存中删除
 * 备注--> value:使用的缓存名称; key:数据在缓存中存储的键
 *         举例:value为list<Map<String,Object>>的名字(引用),key为集合中的一个键
 *         不管是删除还是添加,要先匹配value再匹配key
 */
@Service
public class DemoServiceImpl implements DemoService{
    @Autowired
    @Qualifier("demoPersonRepository")
    private DemoPersonRepository demoPersonRepository;
    @Resource
    private RedisUtil redisUtil;

    @Override
    @CachePut(value = "people",key = "#person.cacheKey")  //1.
    public DemoPerson save(DemoPerson person) {
        DemoPerson p = demoPersonRepository.save(person);
        Long id = p.getId();
        p.setCacheKey(String.valueOf(id));
        demoPersonRepository.saveAndFlush(p);
//        demoPersonRepository.update(String.valueOf(id),id);
        System.out.println("为id和key为:--<"+person.getCacheKey()+">--数据做了缓存");
        return p;
    }

    @Override
    @CacheEvict(value = "people")   //2.
    public void remove(Long id) {
        demoPersonRepository.delete(id);
        System.out.println("删除了id和key为:--<"+id+"-->的数据缓存");
    }

    @Override
    @Cacheable(value = "people",key = "#person.cacheKey") //3.
    public DemoPerson findOne(DemoPerson person) {
        DemoPerson p = demoPersonRepository.findOne(person.getId());
        System.out.println("为id和key为:--<"+person.getId()+">--数据做了缓存");
        redisUtil.set("sf","dddddddddddd");
        return p;
    }
}
