package com.boot.crm.springBoot.cache;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ys on 2018/9/3.
 */
public interface DemoPersonRepository extends JpaRepository<DemoPerson,Long> {
    //1.使用方法名查询，接受一个name参数，返回值为列表
    List<DemoPerson> findByAddress(String name);

    @Transactional
    @Query(value = "update DemoPerson d set d.cacheKey=:cacheKey where d.id=:id")
    int update(@Param("cacheKey")String cacheKey,@Param("id")long id);
}
