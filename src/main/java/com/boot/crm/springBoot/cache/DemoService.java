package com.boot.crm.springBoot.cache;


/**
 * Created by ys on 2018/9/3.
 */
public interface DemoService {
    public DemoPerson save(DemoPerson person);

    public void remove(Long id);

    public DemoPerson findOne(DemoPerson person);
}
