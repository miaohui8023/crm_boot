package com.boot.crm.springBoot.thymeleaf;

import com.boot.crm.doMain.Template;
import com.boot.crm.service.template.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by ys on 2018/6/4.
 */
@Controller
@RequestMapping("/thymeleaf")
public class Thymeleaf {
    @Autowired
    private TemplateService templateService;

    @RequestMapping("/testThymeleaf")
    public String testThymeleaf(Model model){
        model.addAttribute("yang","shen");
        List<Template> templateList = templateService.getAll(null);
        Template template = new Template();
        template.setTemplateLayoutName("一生化模板");
        template.setTemplateLayoutCode("A0001");
        model.addAttribute("templateList",templateList);
        model.addAttribute("template",template);
        return "springBoot/thymeleaf/thymeleafTest";
    }
}
