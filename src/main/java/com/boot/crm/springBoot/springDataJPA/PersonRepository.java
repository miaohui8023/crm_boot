package com.boot.crm.springBoot.springDataJPA;

import com.boot.crm.springBoot.springDataJPA.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ys on 2018/8/25.
 * 此处方法名引用的属性名 要和 实体类一致
 * 3.和4. 效果一样 均为自定义查询方法
 */
public interface PersonRepository extends JpaRepository<Person,Long> {
    //1.使用方法名查询，接受一个name参数，返回值为列表
    List<Person> findByAddress(String name);

    //2.使用方法名查询，接受name和address,返回为单个对象(若存在多个会报错)
    Person findByNameAndAddress(String name1 , String address1);

    //3.使用@Query查询，参数按照名称绑定
    @Query("select p from Person p where p.name=:name and p.address=:address")
    Person withNameAndAddressQuery(@Param("name") String name , @Param("address") String address);

    //4.使用@NamedQuery查询，请注意我们在实体类中做的@NamedQuery的定义
    Person withNameAndAddressNamedQuery(String name , String address);
}
