package com.boot.crm.springBoot.springDataJPA;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by ys on 2018/8/26.
 * 1.@EnableJpaRepositories接收的参数value参数用来扫描数据访问层所在包下的 数据访问的接口(继承JpaRepository)
 */
@Configuration
@EnableJpaRepositories({"com.boot.crm.springBoot.springDataJPA","com.boot.crm.springBoot.cache"}) //1.
public class JpaConfiguration {
//    @Bean
//    public EntityManagerFactory entityManagerFactory(){
//
//    }
}
