package com.boot.crm.other.webService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.Date;

/**
 * @Date:Create in 2018/7/31 12:41
 * 在命令行进入JDK/bin目录执行
 * wsimport -s 盘符目录 -keep 发布路径?wsdl
 * wsimport -s d:\test -keep http://172.16.25.111:9090/crm_boot/service/serviceHello?wsdl
 * webservice 流程
 *  1.编写webservice接口，本接口中exclude方法和静态方法排除发布之外
 *  2.执行节点发布，服务地址和提供服务的类进行一一对应
 *  3.cmd进入JDK/bin目录执行命令，生成编译代码
 *  4.调用编译代码调用目标接口，执行目标接口
 */
@WebService
public class JwsServiceHello {
    /**
     * 供客户端调用方法，该方法时非静态的，会被发布
     */
    public String getValue(String name){
        return "欢迎："+name;
    }
    public String getCurrentTime(){
        return String.valueOf(new Date().getTime());
    }


    /**
     * 反例
     * 在方法上加上@WebMethod(exclude = true)，此方法不会被发布
     * @param name
     * @return
     */
    @WebMethod(exclude = true)
    public String getHello(String name){
        return "你好："+name;
    }

    /**
     * 反例
     * 静态方法不被发布
     */
    public static String getString(String name){
        return name;
    }

    /**
     * 通过endpoint(端点服务)发布一个webservice
     * Endpoint是JDK提供的一个专门用来发布服务的类，接收两个参数
     *          一个是本地的服务地址，一个是提供服务的类
     * @param args
     */
    public static void main(String[] args) {
        Endpoint.publish("http://172.16.25.111:9090/crm_boot/service/serviceHello",new JwsServiceHello());
        System.out.println("发布成功");
    }
}
