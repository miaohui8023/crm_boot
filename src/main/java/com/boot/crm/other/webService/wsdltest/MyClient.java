package com.boot.crm.other.webService.wsdltest;

/**
 * @Author: ys
 * @Date:Create in 2018/8/1 17:20
 * @Description:
 */
public class MyClient {
    public static void main(String[] args) {
        //创建一个用于生产JwsServiceHello实例的工厂，JwsServiceHelloService是wsimport工具生成的
        JwsServiceHelloService service = new JwsServiceHelloService();
        //通过工厂生成一个JwsServiceHello实例，JwsServiceHello是wsimport工具生成的
        JwsServiceHello jwsServiceHelloPort = service.getJwsServiceHelloPort();
        //调用JwsServiceHello中getValue方法
        String ssssssssssss = jwsServiceHelloPort.getValue("ssssssssssss");
        String currentTime = jwsServiceHelloPort.getCurrentTime();
        System.out.println(ssssssssssss);
        System.out.println(currentTime);
    }
}
