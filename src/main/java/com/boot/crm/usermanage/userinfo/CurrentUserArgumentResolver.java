package com.boot.crm.usermanage.userinfo;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ys on 2018/8/8 14:07
 */
public class CurrentUserArgumentResolver implements WebArgumentResolver {
    @Override
    public Object resolveArgument(MethodParameter methodParameter, NativeWebRequest webRequest) throws Exception {
        if (methodParameter.getParameterType()!=null){
            HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
            Object attribute = request.getSession().getAttribute("current_user");
            return attribute;
        }
        return null;
    }
}
