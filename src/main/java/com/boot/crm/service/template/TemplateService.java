package com.boot.crm.service.template;

import com.boot.crm.doMain.Template;

import java.util.List;
import java.util.Map;

/**
 * @Author: ys
 * @Date:Create in 2018/6/1 14:52
 * @Description:
 */
public interface TemplateService {
    List<Template> getAll(Map<String,Object> map);
    List<Template> getAllByPage(int pageNum,int pageSize);
    List<Template> getListMapTemplate();


    //新增
    boolean insertTemplate(Template template);

    //删除
    int delete(List list);

    //修改
    boolean updateTemplate(Template template);
}
