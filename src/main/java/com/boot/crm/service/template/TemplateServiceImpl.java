package com.boot.crm.service.template;

import com.boot.crm.dao.TemplateMapper;
import com.boot.crm.doMain.Template;
import com.boot.crm.plat.util.TimeUtil;
import com.boot.crm.service.template.TemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: ys
 * @Date:Create in 2018/6/1 14:54
 * @Description:
 */
@Service
@Transactional
public class TemplateServiceImpl implements TemplateService {
    @Autowired
    private TemplateMapper templateMapper;
    @Override
    public List<Template> getAll(Map<String,Object> map) {
         return templateMapper.getAll(map);
    }

    @Override
    public List<Template> getAllByPage(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return templateMapper.getAll(new HashMap<>());
    }

    @Cacheable(value = "getListTemplate",key = "'User:'+#id")
    @Override
    public List<Template> getListMapTemplate() {
        List<Template> list = templateMapper.getListMapTemplate();
        return templateMapper.getListMapTemplate();
    }



    @Override
    public boolean insertTemplate(Template template) {
        template.setTemplateLayoutId(UUID.randomUUID().toString());
        template.setCreateTime(TimeUtil.getCurrentTime());
        int i = templateMapper.insertTemplate(template);
        return i==1;
    }

    @Override
    public int delete(List list) {
        return templateMapper.deleteTemplate(list);
    }

    @Override
    public boolean updateTemplate(Template template) {
        int i = templateMapper.updateTemplate(template);
        return i == 1;
    }


}
