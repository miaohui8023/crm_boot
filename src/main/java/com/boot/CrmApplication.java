package com.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ys on 2018/5/2.
 * 1.@SpringBootApplication 是SpringBoot项目的核心注解,主要目的是开启自动配置
 * 2.项目启动的入口
 * 3.在启动类上添加@EnableScheduling注解即可开启 定时
 * 4.在springBoot中需要该注解开启缓存支持
 */
@Controller
@SpringBootApplication	//1.
//@ComponentScan(nameGenerator = UniqueNameGenerator.class)
@MapperScan(basePackages = "com.boot.crm.dao")
@EnableScheduling  	//3.
@EnableCaching	//4.
public class CrmApplication {

	@RequestMapping("/")
	public String login(){
		return "public/index";
	}

	public static void main(String[] args) {	//2.
		SpringApplication.run(CrmApplication.class, args);//启动SpringBoot应用项目
	}

//	/// <summary>
//	/// 获取公网IP
//	/// </summary>
//	/// <returns></returns>
//	private static String InternetIP(){
//		String IP = "";
//
//		HttpWebRequest ReqIP = (HttpWebRequest)HttpWebRequest.Create(new Uri(@"http://ip.qq.com/"));
//		HttpWebResponse ResIP = (HttpWebResponse)ReqIP.GetResponse();
//		Stream s = ResIP.GetResponseStream();
//		StreamReader sr = new StreamReader(s, Encoding.Default);
//		string line = sr.ReadToEnd();
//
//		Regex r = new Regex(@"((25[0-5]|2[0-4]\d|[01]?\d?\d)\.){3}(25[0-5]|2[0-4]\d|[01]?\d?\d)", RegexOptions.None);//设置正则表达式
//		Match m = r.Match(line);//对指定的字符串进行正则表达式匹配
//
//		IP = m.Groups[0].Value.ToString();
//		ResIP.Close();
//		return IP;
//	}
}
