/**
 * Created by ys on 2018/8/31.
 * jquery使用到的类选择器记录
 */

//单选框或者复选框中会有一个value属性，下述为获取选中的单选/复选 的value属性对应的---属性值
$("#_div_userClass :input[type='radio']:checked").val();
//获取所有选中的复选框
$("input[type='checkbox']:checked").map(function(index,item){
    console.log($(item).val());
});

//根据属性获取 (可以获取集合也可以获取指定第几个)
$("[name='userClass']:eq(1)");

//代码控制单选/复选框是否选中
$("#_div_userClass :input[type='checkbox']:input[name='member']").attr("checked","checked/false");