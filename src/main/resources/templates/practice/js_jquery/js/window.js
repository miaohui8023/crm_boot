/**
 * Created by ys on 2018/8/22.
 */
/**1.在index.html中设置，应该是在项目访问路径后面拼接下述路径**/
window.location = "index.htm";//访问路径

//护理详情
$(".nuring_info").click(function(){
    window.location.replace("护理详情.html")
    /* window.location.href="护理详情.html";*/
});

/**3.拼接地址(get请求)，执行顺序
    举例：localhost:9097/ltcins-intact-cms 或者 localhost:9097/ltcins-intact-cms/main.htm
    执行之后则为：localhost:9097/ltcins-intact-cms/login.htm
 */
 top.location.href = 'login.htm';

 /**4.返回上一页面 **/
 window.history.go(-1);