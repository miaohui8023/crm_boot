$(function(){
	//根据窗口调整表格高度
    $(window).resize(function() {
        $('#mytab').bootstrapTable('resetView', {
            height: tableHeight()
        })
    });
    //展示表格数据
    $('#mytab').bootstrapTable({
    	method: 'post',
    	contentType: "application/x-www-form-urlencoded",
    	url:rootPath+"master/getAllTemplate",
    	height:tableHeight(),//高度调整
    	striped: true, //是否显示行间隔色
    	dataField: "res",
    	pageNumber: 1, //初始化加载第一页，默认第一页
    	pagination:true,//是否分页
    	queryParamsType:'limit',
    	queryParams:queryParams,
    	sidePagination:'client',
    	pageSize:10,//单页记录数
    	pageList:[5,10,20,30],//分页步进值
    	showRefresh:true,//刷新按钮
    	showColumns:true,
    	clickToSelect: true,//是否启用点击选中行
    	toolbarAlign:'right',
    	buttonsAlign:'right',//按钮对齐方式
    	toolbar:'#toolbar',//指定工作栏
    	columns:[
        	{
        		title:'全选',
        		field:'select',
        		checkbox:true,
        		width:25,
        		align:'center',
        		valign:'middle'
        	},
        	{
        		title:'主键',
        		field:'templateLayoutId',
        		visible:false
        	},
        	{
        		title:'模板编号',
        		field:'templateLayoutCode',
        		sortable:true
        	},
        	{
        		title:'模板名称',
        		field:'templateLayoutName',
        		sortable:true
        	},
        	{
        		title:'状态',
        		field:'state',
        	},
        	{
        		title:'模板类型',
        		field:'templateLayoutType'
        	},
        	{
        		title:'部门',
        		field:'templateLayoutOrg',
        		sortable:true
        	},
        	{
        		title:'状态',
        		field:'Attribute',
        		align:'center',
        		formatter:operateFormatter
        	}
    	],
    	locale:'zh-CN'  //中文支持,
    });
    /*
     * 用户管理首页事件
     */
   function operateFormatter(value,row,index){
    	if(value==2){
    		return '<i class="fa fa-lock" style="color:red">锁定</i>'
    	}else if(value==1){
    		return '<i class="fa fa-unlock" style="color:green">开锁</i>'
    	}else{
    		return '数据错误'
    	}
    }

    //请求服务数据时所传参数
    function queryParams(params){
   		var res={
            pageSize: params.limit,
            pageIndex:params.pageNumber,
            templateLayoutCode:$('#templateLayoutCode').val(),
            templateLayoutName:$('#templateLayoutName').val()
		}
		var result=JSON.stringify(res);
    	return res;
    }
    //查询按钮事件
    $('#search_btn').click(function(){
    	$('#mytab').bootstrapTable('refresh', {url: rootPath+'master/getAllTemplate'});
    })
    
    //增加按钮事件
    $('#btn_add').click(function(){
		$('.tableBody').addClass('animated slideOutLeft');
		setTimeout(function(){
			$('.tableBody').removeClass('animated slideOutLeft').css('display','none');
		},500);
		$('.addBody').css('display','block');
		$('.addBody').addClass('animated slideInRight');
    })
    //删除按钮与修改按钮的出现与消失
    $('.bootstrap-table').change(function(){
    	var dataArr=$('#mytab .selected');
    	if(dataArr.length==1){
    		$('#btn_edit').css('display','block').removeClass('fadeOutRight').addClass('animated fadeInRight');
    	}else{
    		$('#btn_edit').addClass('fadeOutRight');
    		setTimeout(function(){
    			$('#btn_edit').css('display','none');
    		},400);	
    	}
    	if(dataArr.length>=1){
    		$('#btn_delete').css('display','block').removeClass('fadeOutRight').addClass('animated fadeInRight');
    	}else{
    		$('#btn_delete').addClass('fadeOutRight');
    		setTimeout(function(){
    			$('#btn_delete').css('display','none');
    		},400);	
    	}
    });
    //修改按钮事件
    $('#btn_edit').click(function(){
    	var dataArr=$('#mytab').bootstrapTable('getSelections');
    	$('.tableBody').addClass('animated slideOutLeft');
		setTimeout(function(){
			$('.tableBody').removeClass('animated slideOutLeft').css('display','none');
		},500)
		$('.changeBody').css('display','block');
		$('.changeBody').addClass('animated slideInRight');
		$('#edit_templateLayoutId').val(dataArr[0].templateLayoutId);
		$('#edit_templateLayoutCode').val(dataArr[0].templateLayoutCode);
		$('#edit_templateLayoutName').val(dataArr[0].templateLayoutName);
		$('#edit_state').val(dataArr[0].state);
		$('#edit_templateLayoutType').val(dataArr[0].templateLayoutType);
		$('#edit_templateLayoutOrg').val(dataArr[0].templateLayoutOrg);
		if(dataArr[0].Attribute==1){
			$("#editForm input[name=Attribute]:eq(0)").prop("checked",true);
			$("#editForm input[name=Attribute]:eq(1)").prop("checked",false);
		}
		else if(dataArr[0].Attribute==2){
			$("#editForm input[name=Attribute]:eq(1)").prop("checked",true);
			$("#editForm input[name=Attribute]:eq(0)").prop("checked",false);
		}
		//先清空角色复选框
	    $('#editForm .edit input').prop('checked',false);
		//获取用户角色
		$.post('../index.php/admin/Index/getUserById',
				{ID:dataArr[0].ID},
				function(data){
				   var roleIDArr=data.res.user.RoleID;
                   //将对应用户的角色列表显示到对应的修改页
                   for(var i=0;i<roleIDArr.length;i++){
                	   for(var j=0;j<$('#editForm .edit input').length;j++){
                		   if(roleIDArr[i]==$('#editForm .edit input:eq('+j+')').val()){
                			   $('#editForm .edit input:eq('+j+')').prop('checked',true);
                		   }
                	   }
                   }
		        }
		);
    })
    /*
     * 用户管理增加用户页面所有事件
    */
    //增加页面表单验证   保存
    // Validate the form manually
    $('#add_saveBtn').click(function() {
       //点击保存时触发表单验证
       $('#addForm').bootstrapValidator('validate');
       //如果表单验证正确，则请求后台添加用户
       if($("#addForm").data('bootstrapValidator').isValid()){
    	   var _info = $('#addForm').serialize();
    	  $.post(
				rootPath+"master/insertTemplate",
				$('#addForm').serialize(),
				function(data){
					//后台返回添加成功
					if(data==true){
						$('.addBody').addClass('animated slideOutLeft');
                        setTimeout(function(){
							$('.addBody').removeClass('animated slideOutLeft').css('display','none');
						},500);
                        $('.tableBody').css('display','block').addClass('animated slideInRight');
                        $('#mytab').bootstrapTable('refresh', {url: rootPath+'master/getAllTemplate'});
                        $('#addForm').data('bootstrapValidator').resetForm(true);
                        //隐藏修改与删除按钮
						$('#btn_delete').css('display','none');
                        $('#btn_edit').css('display','none');
                        alert("新增成功");
                    }
					//否则
					else{
                    }
				}
			) 
       }
    });
    //增加页面返回按钮事件
    $('#add_backBtn').click(function() {
    	$('.addBody').addClass('animated slideOutLeft');
    	setTimeout(function(){
			$('.addBody').removeClass('animated slideOutLeft').css('display','none');
		},500)
    	$('.tableBody').css('display','block').addClass('animated slideInRight');  
    	$('#addForm').data('bootstrapValidator').resetForm(true);
    });
    /*
     * 用户管理修改用户页面所有事件
    */
    //修改页面回退按钮事件
    $('#edit_backBtn').click(function(){
    	$('.changeBody').addClass('animated slideOutLeft');
    	setTimeout(function(){
			$('.changeBody').removeClass('animated slideOutLeft').css('display','none');
		},500)
    	$('.tableBody').css('display','block').addClass('animated slideInRight'); 
    	$('#editForm').data('bootstrapValidator').resetForm(true);
    })
    //修改页面保存按钮事件
    $('#edit_saveBtn').click(function(){
    	$('#editForm').bootstrapValidator('validate');
    	if($("#editForm").data('bootstrapValidator').isValid()){
    		 $.post(rootPath+"master/updateTemplate",
				$('#editForm').serialize(),
				function(data){
					if(data==true){
						//隐藏修改与删除按钮
						$('#btn_delete').css('display','none');
						$('#btn_edit').css('display','none');
						//回退到人员管理主页
						$('.changeBody').addClass('animated slideOutLeft');
				    	setTimeout(function(){
							$('.changeBody').removeClass('animated slideOutLeft').css('display','none');
						},500)
				    	$('.tableBody').css('display','block').addClass('animated slideInRight'); 
				    	//刷新人员管理主页
				    	$('#mytab').bootstrapTable('refresh', {url: rootPath+"master/getAllTemplate"});
				    	//修改页面表单重置
				    	$('#editForm').data('bootstrapValidator').resetForm(true);
					}else{
					}
			    }
    		 ) 
    	}
    })
    //删除事件按钮
    $('#btn_delete').click(function(){
    	var dataArr=$('#mytab').bootstrapTable('getSelections');
    	$('.popup_de .show_msg').text('确定要删除该用户吗?');
    	$('.popup_de').addClass('bbox');
    	$('.popup_de .btn_submit').one('click',function(){
    		var rowIds=[];
        	for(var i=0;i<dataArr.length;i++){
                rowIds[i]=dataArr[i].templateLayoutId;
        	}
        	$.post(rootPath+"master/deleteTemplate",
        			{rowIds:JSON.stringify(rowIds)},
        			function(data){
        		if(data==rowIds.length){
        			$('.popup_de .show_msg').text('删除成功！');
					$('.popup_de .btn_cancel').css('display','none');
					$('.popup_de').addClass('bbox');
					$('.popup_de .btn_submit').one('click',function(){
						$('.popup_de').removeClass('bbox');
					});
        			// $('#mytab').bootstrapTable('refresh', {url: rootPath+'master/showMasterMainPage'});
        			// $('#mytab').bootstrapTable('refresh', {url: '../index.php/admin/index/userManagement'});
                    window.location.href=rootPath+'master/showMasterMainPage';
        		}else{
        		}
        	});
    	})
    })
    //弹出框取消按钮事件
   　　$('.popup_de .btn_cancel').click(function(){
	   $('.popup_de').removeClass('bbox');
   　　})
    //弹出框关闭按钮事件
     $('.popup_de .popup_close').click(function(){
	   $('.popup_de').removeClass('bbox');
   　　})
})
function tableHeight() {
    return $(window).height() - 140;
}
